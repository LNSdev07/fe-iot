import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { NzPageHeaderModule } from 'ng-zorro-antd/page-header';
import { NgChartsModule } from 'ng2-charts';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzInputNumberModule } from 'ng-zorro-antd/input-number';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzSwitchModule } from 'ng-zorro-antd/switch';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzFormModule } from 'ng-zorro-antd/form';


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  exports :[
    NzTableModule,
    NzFormModule,
    FormsModule,
    NzInputModule,
    NzButtonModule,
    NzSwitchModule,
    HttpClientModule,
    NzAvatarModule,
    NzPageHeaderModule,
    // BrowserAnimationsModule,
    NzLayoutModule,
    NzMenuModule,
    NzCardModule,
    NgChartsModule,
    NzDatePickerModule,
    HttpClientModule,
    NzTagModule,
    NzSelectModule,
    NzInputNumberModule,
    NzModalModule,
    NzMessageModule,
  ],
  providers: [
    { provide: NZ_I18N, useValue: en_US }
  ]
})
export class SharedModule { }
